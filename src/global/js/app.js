function App(props) {

  React.useEffect(() => {
    if (location.hostname === Config.env.live.location_hostname) {
      console.log = () => {};
    }
  }, []);

  let keyIncrement = 0;
  const [input, setInput] = React.useState('');
  const [focus, setFocus] = React.useState(null);

  function onChange(e) {
    setInput(e.target.value);
    setFocus(null);
  }

  function getKey() {
    return (keyIncrement++);
  }

  function onClickKey(key, e) {
    e.stopPropagation();
    setFocus(key);
  }

  function onClickBackground() {
    setFocus(null);
  }

  function convertToHTML(value, key = null, level = 0, hilightLevel = null) {
    let output = [];

    let type;
    let brackets = [null, null];
    if (Array.isArray(value)) {
      type = 'array';
      brackets = ['[\n',']'];
    }
    else if (typeof value === 'object' && value !== null) {
      type = 'object';
      brackets = ['{\n','}'];
    }
    else if (value === null) {
      type = 'null';
    }
    else {
      type = typeof value;
    }

    for (let j=0; j<level; j++) {
      output.push(<span key={getKey()} className={`space ${(j === hilightLevel - 1) ? 'hilight' : ''}`}>{'. '}</span>);
    }

    const keyKey = getKey();
    if (key !== null || brackets[0] !== null) {
      let classHilightKey = '';
      if (keyKey === focus) {
        classHilightKey = 'hilight';
        hilightLevel = level + 1;
      }
      if (typeof key === 'number') {
        output.push(<span key={keyKey} onClick={onClickKey.bind(null, keyKey)} className={classHilightKey}>
          <span className='grey'>{(key !== null ? (key + ': ') : '')}</span>
          <span>{(brackets[0] || '')}</span>
        </span>)
      }
      else {
        output.push(<span key={keyKey} onClick={onClickKey.bind(null, keyKey)} className={classHilightKey}>{(key !== null ? (key + ': ') : '') + (brackets[0] || '')}</span>)
      }
    }

    switch(type) {
      case 'object': {
        Object.keys(value).forEach((sub_key) => {
          output.push(...convertToHTML(value[sub_key], sub_key, level + 1, hilightLevel));
        });
        break;
      }
      case 'array': {
        value.forEach((sub_value, i) => {
          output.push(...convertToHTML(sub_value, i, level + 1, hilightLevel));
        });
        break;
      }
      case 'string': {
        output.push(<span key={getKey(level)} className="red">{'"' + value + '"'}</span>);
        break;
      }
      case 'number': {
        output.push(<span key={getKey(level)} className="yellow">{value.toString()}</span>);
        break;
      }
      case 'boolean': {
        output.push(<span key={getKey(level)} className="green">{value.toString()}</span>);
        break;
      }
      case 'null': {
        output.push(<span key={getKey(level)} className="green">{'null'}</span>);
        break;
      }
    }

    switch(type) {
      case 'object':
      case 'array': {
        for (let j=0; j<level; j++) {
          output.push(<span key={getKey()} className={`space ${(j === hilightLevel - 1) ? 'hilight' : ''}`}>{'. '}</span>);
        }
        output.push(<span key={getKey()} onClick={onClickKey.bind(null, keyKey)} className={(level === hilightLevel - 1) ? 'hilight' : ''}>{brackets[1]}</span>)
        break;
      }
    }

    output.push(<span key={getKey(level)} onClick={onClickKey.bind(null, keyKey)}>{',\n'}</span>);

    return output;
  }

  let output;
  if (input !== '') {
    let data;
    let error;
    try {
      data = JSON.parse(input);
    }
    catch(err) {
      error = err.message;
    }
    if (data !== undefined) {
      output = convertToHTML(data);
    }
    else {
      const window = 20;
      const index = Number(error.split(' ').pop());
      let a = index - window;
      let b = index + window;
      if (a < 0) {
        a = 0;
      }
      if (b > input.length - 1) {
        b = input.length - 1;
      }
      output = (
        <>
          <span>{error + ' near:\n\n'}</span>
          <span className="red">{input.substring(a, b)}</span>
        </>
      );
    }
    if (Array.isArray(output)) {
      output.pop();
    }
  }

  return (
    <>
      <div className="input">
        <div className="header">
          <h1>{'json-viewer'}</h1>
        </div>
        <textarea
          className="input chalk"
          value={input}
          onChange={onChange}
          placeholder={'insert JSON here'}
        />
      </div>
      <div className="output chalk" onClick={onClickBackground}>
        <pre>
          {output}
        </pre>
      </div>
    </>
  );

};

ReactDOM.render(<App/>, document.getElementById('json-viewer'));
