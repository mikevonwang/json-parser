export default class Utilities {

  static setCookie(key, value) {
    document.cookie = key + '=' + value + '; expires=Fri, 31 Dec 9999 23:59:59 GMT';
  }

  static getCookie(key) {
    const cookies = document.cookie.split(';');
    let crumbs;
    for (let i=0; i<cookies.length; i++) {
      crumbs = cookies[i].split('=');
      if (crumbs[0] === 'lang') {
        return (crumbs[1]);
      }
    }
    return undefined;
  }

  static get_date_displayer(lang) {
    return function display_date(date_raw) {
      const date_parts = date_raw.split('-');
      if (lang === 'ra') {
        return (Utilities.base10to12(Number(date_parts[0])) + ' ' + months[lang][Number(date_parts[1])-1] + ' ' + Utilities.pad(Utilities.base10to12(Number(date_parts[2]))));
      }
      else {
        return (date_parts[0] + ' ' + months[lang][Number(date_parts[1])-1] + ' ' + date_parts[2]);
      }
    };
  }

  static pad(input) {
    if (input < 10) {
      return '0' + input;
    }
    else {
      return input.toString();
    }
  }

  static base10to12(input) {
    let output = '';
    let a = input;
    while (a >= 1) {
      const b = a % 12;
      if (b < 10) {
        output = b + output;
      }
      else if (b === 10) {
        output = '∂'+ output;
      }
      else if (b === 11) {
        output = 'Ɛ' + output;
      }
      a = Math.floor(a / 12);
    }
    return output;
  }

  static filter_unpublished_posts(posts) {
    if (Config.env.local.location_hostname === location.hostname) {
      return posts;
    }
    else {
      return posts.filter((post) => {
        return (post.published === true);
      });
    }
  }

  static add_date_to_posts(posts) {
    return posts.map((post) => {
      const new_localized = {};
      Object.keys(post.localized).forEach((lang) => {
        new_localized[lang] = Object.assign({}, post.localized[lang], {
          date: Utilities.get_date_displayer(lang),
        });
      });
      return Object.assign({}, post, {
        localized: Localizer(new_localized),
      });
    });
  }

}

const months = {
  en: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
  de: ['Jan', 'Feb', 'Mär', 'Apr', 'Mai', 'Jun', 'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Dez'],
  ra: ['jan', 'fev', 'mar', 'avr', 'maï', 'jun', 'jul', 'aug', 'sev', 'oko', 'nov', 'dec'],
};
